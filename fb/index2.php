<?php

require __DIR__ . '/vendor/autoload.php';

use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\ServerSide\ActionSource;
use FacebookAds\Object\ServerSide\Content;
use FacebookAds\Object\ServerSide\CustomData;
use FacebookAds\Object\ServerSide\DeliveryCategory;
use FacebookAds\Object\ServerSide\Event;
use FacebookAds\Object\ServerSide\EventRequest;
use FacebookAds\Object\ServerSide\UserData;

$access_token = $_GET['token'];
$pixel_id = $_GET['pixel'];

$current_timestamp = time();
$fbclid = $_GET['fbclid'];
$fbp = $_COOKIE['_fbp'] ? $_COOKIE['_fbp'] : 'fbp';
$fbc = 'fb.1.'.$current_timestamp.'.'.$fbclid;
$event_name = $_GET['event'];
$eventId = $_GET['event_id'];
$country = 'a56145270ce6b3bebd1dd012b73948677dd618d496488bc608a3cb43ce3547dd';

$api = Api::init(null, null, $access_token);

$user_data = (new UserData())
    ->setClientIpAddress($_SERVER['REMOTE_ADDR'])
    ->setClientUserAgent($_SERVER['HTTP_USER_AGENT'])
    ->setFbc($fbc)
    ->setFbp($fbp);

$event = (new Event())
    ->setEventName($event_name)
    ->setEventTime(time())
    ->setEventSourceUrl('https:marrli.com/fb')
    ->setUserData($user_data)
    ->setActionSource(ActionSource::WEBSITE);

$events = array();
array_push($events, $event);

$request = (new EventRequest($pixel_id))
    ->setEvents($events);
$response = $request->execute();

$result = $response['container:protected'];

$data = [
    'result' => [
        'pixel' => $pixel_id,
        'token' => $access_token,
        'event' => $event_name
    ],
    'data' => 'ok'
];
$json = json_encode($data);

echo $json;
